This app packages Slingcode with Surfer <upstream>5.11.3</upstream>

### Overview

 Slingcode - Personal computing platform in a single HTML file 

### Webinterface

 * Edit HTML
 * Upload files
 * Create directories
 * Remove files and directories
