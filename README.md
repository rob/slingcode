# Slingcode

Slingcode is a personal computing platform in a single html file.

    - You can make, run, and share web apps with it.
    - You don't need any complicated tools to use it, just a web browser.
    - You don't need a server, hosting, or an SSL certificate to run the web apps.
    - You can put Slingcode on a web site, run it from a USB stick, laptop, or phone, and it doesn't need an internet connection to work.
    - You can "add to home screen" in your phone's browser to easily access your library of programs on the go.
    - You can share apps peer-to-peer over WebTorrent.
    - It's private. You only share what you choose.


## Installation

[![Install](https://cloudron.io/img/button32.png)](https://cloudron.io/button.html?app=io.toldyouso.slingcode)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id io.toldyouso.slingcode
```

## Building

### Cloudron
The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
git clone https://git.cloudron.io/rob/sligncode/slingcode.git
cd slingcode
cloudron build
cloudron install
```

### Standalone
Surfer can also be run standlone on any server:
```
git clone https://git.cloudron.io/cloudron/surfer.git
cd surfer
npm install
./server.js <public folder>
```
Use the `admin` tool to manage local users.

## File management

The admin interface is available under the `/_admin` location or you can upload files using the commandline tool.

First, install the surfer cli tool using npm.
```
npm -g install cloudron-surfer
```

Login using your Cloudron credentials:
```
surfer login <this app's url>
```

Put some files:
```
surfer put [file]
```

## Testing

The e2e tests are located in the `test/` folder and require [nodejs](http://nodejs.org/). They are creating a fresh build, install the app on your Cloudron, perform tests, backup, restore and test if the files are still ok.

```
cd surfer

npm install
USERNAME=<cloudron username> PASSWORD=<cloudron password> mocha --bail test/test.js
```
